# autonyaa

![](https://img.shields.io/badge/written%20in-Bash-blue)

Shell script to download torrents from nyaa and automatically add them to transmission.

Tags: torrent, anime

## Usage


```
Usage:
  autonyaa [flags] [search [search2 ...]]

Options:
  --dry-run               Do not download torrent files
  --filelist FILENAME     Search all terms in the file FILENAME.
                           One term per line, # to comment
  --help                  Display this message
  --sukebei               Use alternative nyaa host
  -v                      Verbose

Environment:
  TORRENT_FILE_DIR        Set a custom directory to store cached torrent files.
                           Default $HOME/.cache/autonyaa/archive/
  NYAA_HOSTNAME           Set a custom nyaa-compatible data provider
```


## Changelog

2018-01-18 1.3
- Feature: Add `--dry-run` flag
- Compatibility with nyaa.si
- [⬇️ autonyaa-1.3.tar.xz](dist-archive/autonyaa-1.3.tar.xz) *(1.98 KiB)*


2016-10-25 1.2
- Enhancement: Default to HTTPS endpoint
- Enhancement: More diagnostic messages in verbose mode
- Fix an issue with cloudflare-scrape mode
- [⬇️ autonyaa-1.2.tar.xz](dist-archive/autonyaa-1.2.tar.xz) *(1.93 KiB)*


2016-10-09 1.1
- Feature: Get all entries from file
- Feature: Support bypassing CloudFlare browser detection (thanks https://github.com/Anorov/cloudflare-scrape )
- Feature: Sukebei support
- Feature: Custom nyaa targets
- Feature: Custom cache directory, move default to to $HOME/.cache
- Enhancement: Use flag for debug verbosity instead of environment variable
- Enhancement: Rename executable
- Fix an issue with not reporting errors (missing `set -e` and some conditional handling)
- Fix an issue with untarred distribution
- [⬇️ autonyaa-1.1.tar.xz](dist-archive/autonyaa-1.1.tar.xz) *(10.00 KiB)*


2013-06-23 1.0
- Initial release
- [⬇️ autonyaa-1.0.tar.xz](dist-archive/autonyaa-1.0.tar.xz) *(788B)*

